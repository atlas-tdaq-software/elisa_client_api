#ifndef ELISA_CLIENT_API_EXCEPTIONS_H
#define ELISA_CLIENT_API_EXCEPTIONS_H

/*! \file exceptions.h Declares elisa_client_api exceptions
 * \author Alina Corso-Radu
 * \date 16.01.2014 
 */

#include <ers/ers.h>

namespace elisa
{

/*! \class elisa::FailToRun
 * This exception is thrown when api can't run the pyhton script.
 */

ERS_DECLARE_ISSUE(      elisa, 
                        FailToRun, 
                        "Failed to run scripts.", )

/*! \class elisa::ErrorFromREST
 * This exception is thrown when an error is returned from ELisA REST server.
 */

ERS_DECLARE_ISSUE(      elisa, 
                        ErrorFromREST, 
                        "Error received from REST server.", )

}
		    
#endif
