/**
 * \class ELisA_clientAPI
 *
 * \brief C++ wrapper for ELisA client API.
 *
 * This class is meant to be used as a C++ wrapper for ELisA client API.
 *
 * \author $Author: Alina Corso-Radu (jan. 2014)  $
 *
 * \date $Date: Jan. 2014 $
 *
 * Contact: alina.radu@cern.ch
 *
 * Created on: 09.01.2014
 *
 *
 */
#ifndef ELISA_CLIENT_API_H
#define ELISA_CLIENT_API_H
#include <ers/ers.h>
#include "elisa_client_api/c++/exceptions.h"

namespace elisa
{
  class ELisA_clientAPI
  {
    public:
	ELisA_clientAPI() {};
	~ELisA_clientAPI() {};	

	/** \brief Get entries from ElisA logbook. 
	* \param  std::vector<std::string> List of arguments required by $TDAQ_INST_PATH/share/bin/elisa_get utility
	* \return EXIT_FAILURE or EXIT_SUCCESS
	* \throw elisa::FailToRun, elisa::ErrorFromREST 
	* 
	* This method reads messages from ELisA logbook. 
        * Current implementation is executing a script that internally is using ELisA Python API. 
	* 
	* For the full list of options to provide, use 
	* $TDAQ_INST_PATH/share/bin/elisa_get -h
	*/
        int elisa_get(std::vector<std::string>); //throw (elisa::FailToRun, elisa::ErrorFromREST);

	/** \brief Insert entry to ElisA logbook. 
	* \param   std::vector<std::string> List of arguments required by $TDAQ_INST_PATH/share/bin/elisa_insert utility
	* \return EXIT_FAILURE or EXIT_SUCCESS
	* \throw elisa::FailToRun, elisa::ErrorFromREST 
	*
	* This method is used to insert a message to ELisA logbook. 
        * Current implementation is executing a script that internally is using ELisA Python API. 
	* 
	* For the full list of options to provide, use 
	* $TDAQ_INST_PATH/share/bin/elisa_insert -h
	*/
	int elisa_insert(std::vector<std::string>); //throw (elisa::FailToRun, elisa::ErrorFromREST);

	/** \brief Get messages configuration from ELisA logbook. 
	* \param   std::vector<std::string> List of arguments required by $TDAQ_INST_PATH/share/bin/elisa_config utility
	* \return std::map<std::string,std::vector<std::string>> Map of key;vector of string containing all configuration information
	* \throw elisa::FailToRun, elisa::ErrorFromREST
	*
	* This method is used to retrieve messages configuration from ELisA logbook.
	* Before creating and inserting a message the user should get the required options for a specific message and 
	* predefined system affected values. 
	*
	* For the full list of options to provide, use $TDAQ_INST_PATH/share/bin/elisa_config -h
	*
	* Different output will be produced depending on the input parameters:
	* - no additional params -> MT and SA keys are inserted into the output map.
	* - additional -y "message type" -> OPTIONS, INNER-OPTIONS and SA-predefined  keys are inserted into the result map.
	*   
	* The complete output map contains several keys:vector of strings depending on the required configuration.
	*
	* key="MT" => value: all possible values of message types
	* key="SA" => value: all possible values of systems affected
	* key="SA-predefined" => value: predefined, default values for systems affected for a given message type (-y input parameter)
	* key="OPTIONS" => value: vector of one string with a structure like the following, containing top options for a given message type (-y input parameter)
	*  "  Name:            DQ_Type
	*     Type:            SINGLEVALUE
	*     Possible values: online,offline,Muon Run Summary
	*     Comment:         Choose one among the possible values "
	*  N.B. The user has to parse this string and extract the information needed.
	*
	* key="INNER-OPTIONS"=> value: vector of one string with a structure like the following, containing inner options for a given message type (-y input parameter)
	* "      Name:            Combined Performance
	*       Type:            MULTIPLEVALUE
	*       Possible values: Jet/EtMiss,Tau,Muon,E-gamma,CaloGlobal,Flavour Tagging
	*       Comment:         Choose one or more among the possible values , in a comma separated string
	*       Name:            Combined Performance
	*       Type:            MULTIPLEVALUE
	*       Possible values: Jet/EtMiss,Tau,Muon,E-gamma,CaloGlobal,Flavour Tagging
	*       Comment:         Choose one or more among the possible values , in a comma separated string
	*       Name:            RS_RunNumber
	*       Type:            FREETEXT
	*       Possible values: 
	*       Comment:         Free text as input "
	* N.B. The user has to parse this string and extract the information needed.
	*
        * Current implementation is executing a script that internally is using ELisA Python API. 
	*/
	std::map<std::string,std::vector<std::string>> elisa_get_config(std::vector<std::string> ); //throw (elisa::FailToRun, elisa::ErrorFromREST);

   private:
        std::vector<char *> transform_input(std::vector<std::string> & input, std::string elem_to_add);
        std::map<std::string,std::vector<std::string>> handle_child_process_output(char buffer[], ssize_t count);
	void create_child_read_pipes(char* firstArg, char * argvExtend[]);
	
	std::map<std::string,std::vector<std::string>> configM;
  };
}
#endif

