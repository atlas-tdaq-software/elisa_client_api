tdaq_package()

tdaq_add_library(elisa_client_api 
  c++/src/*.cpp
  LINK_LIBRARIES Boost::thread)

tdaq_add_jar(elisaJClientAPI
  java/src/main/java/elisa_client/model/*.java
  java/src/main/java/elisa_client/service/*.java
  java/src/main/resources/applicationContext.xml  
  java/src/main/resources/log4j.properties
  java/src/main/resources/runtime.properties
  INCLUDE_JARS jeformat/jeformat.jar TDAQExtJars/external.jar)

tdaq_add_jar(elisaJClientAPI-Helper_get
  java/src/main/java/elisa_client/helper/Helper_get.java
  java/src/main/resources/applicationContext.xml
  java/src/main/resources/log4j.properties
  java/src/main/resources/runtime.properties
  java/manifest_get.mf 
  INCLUDE_JARS elisa_client_api/elisaJClientAPI.jar jeformat/jeformat.jar TDAQExtJars/external.jar)

tdaq_add_jar(elisaJClientAPI-Helper_insert
  java/src/main/java/elisa_client/helper/Helper_insert.java
  java/src/main/resources/applicationContext.xml
  java/src/main/resources/log4j.properties
  java/src/main/resources/runtime.properties
  java/manifest_insert.mf 
  INCLUDE_JARS elisa_client_api/elisaJClientAPI.jar TDAQExtJars/external.jar)

# hacks, how about just following some simple conventions
install(DIRECTORY python/src/ COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/lib/python/elisa_client_api)

file(GLOB python_files python/bin/*.py)
install(FILES ${python_files} COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/lib/python/elisa_client_api)

tdaq_add_scripts(scripts/*)

tdaq_add_executable(elisa_api_cpp_test_get
  c++/bin/elisa_api_cpp_test_get.cpp
  LINK_LIBRARIES elisa_client_api tdaq-common::ers Boost::thread)

tdaq_add_executable(elisa_api_cpp_test_insert
  c++/bin/elisa_api_cpp_test_insert.cpp
  LINK_LIBRARIES elisa_client_api tdaq-common::ers Boost::thread)

tdaq_add_executable(elisa_api_cpp_test_config
  c++/bin/elisa_api_cpp_test_config.cpp  
  LINK_LIBRARIES elisa_client_api tdaq-common::ers Boost::thread)


