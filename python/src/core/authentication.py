#!/usr/bin/env tdaq_python
#--------------------------------------------------------------------------------------
# Title         : ELisA authentication.
# Project       : ATLAS, TDAQ, ELisA
#--------------------------------------------------------------------------------------
# File          : authentication.py
# Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
# Created       : 18/Dec/2012
# Revision      : 0 $
#--------------------------------------------------------------------------------------
# Class         : Authentication
# Description   : Class providing the functionality to perform user authentication
#                 either using LDAP or SSO. 
#--------------------------------------------------------------------------------------
# Copyright (c) 2012 by University of California, Irvine. All rights reserved.
#--------------------------------------------------------------------------------------
# Modification history:
# 18/Dec/2012: created.
#--------------------------------------------------------------------------------------

from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import object
from elisa_client_api.exception import ArgumentError

from urllib.request import build_opener, HTTPCookieProcessor
import http.cookiejar
import sys
import os

class Authentication(object):
    """ Interface to the ELisA logbook database. 
    """
    def __init__(self, username=None, password=None, ssocookie=None, token=None):
        """ Constructor
            
        username: user name.
        password: password 
        type: authentication type; either 'ldap' or 'sso'.
        ssocookie: file with the sso-cookie created by auth-get-sso-cookie
        token: user token created by auth-get-user-token
        """
        self.__username = username
        self.__password = password
        self.__ssocookie = None
        self.__token = None

        if ssocookie != None:
            try:
                f = open(ssocookie)
                self.__ssocookie = f.read()
                f.close()
            except IOError as ex:
                raise ArgumentError("The cookie containing the sso authentication could not be read: " + str(ex))
        
            # Load the cookie 
            import http.cookiejar
            cj = http.cookiejar.MozillaCookieJar()
            cj.load(filename=ssocookie, ignore_discard=True, ignore_expires=True)
            for cookie in cj:
               # if True == cookie.name.startswith('_shibsession'):
               if cookie.name.startswith('_shibsession') or cookie.name == 'mod_auth_openidc_session':
                    self.__ssocookie = cookie.name + '=' + cookie.value

        if token != None:
            try:
                f = open(token)
                self.__token = f.read()
                f.close()
            except IOError as ex:
                raise ArgumentError("The token containing the authorization could not be read: " + str(ex))


    # ------------------
    # - Public methods -
    # ------------------
    def addAuthentication(self,req):
        """ Adds the appropriate authentication header in the http request.
        """
        if self.__ssocookie != None:
            req.headers['Cookie'] = self.__ssocookie
        elif self.__token != None:
            req.headers["Authorization"] = "Bearer %s" % self.__token
        else:
            req.headers["Authorization"] = "Basic %s" % self._getEncodedCredentials().decode()
            
        
    # -------------------
    # - Private methods -
    # -------------------
    def _getEncodedCredentials(self):
        """ Encodes the string username:password with basic 64
        """
        import base64
        return base64.encodebytes(('%s:%s' % (self.__username, self.__password)).encode())[:-1]
