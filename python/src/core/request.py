#!/usr/bin/env tdaq_python
#--------------------------------------------------------------------------------------
# Title         : HTTP request
# Project       : ATLAS, TDAQ, ELisA
#--------------------------------------------------------------------------------------
# File          : request.py
# Author        : Raul Murillo Garcia, raul.murillo.garcia@cern.ch
# Created       : 19/Dec/2012
# Revision      : 0 $
#--------------------------------------------------------------------------------------
# Class         : Request
# Description   : Encapsulates the functionality to perform HTTP requests. 
#--------------------------------------------------------------------------------------
# Copyright (c) 2012 by University of California, Irvine. All rights reserved.
#--------------------------------------------------------------------------------------
# Modification history:
# 19/Dec/2012: created.
# 08/Jan/2012: add authentication
#--------------------------------------------------------------------------------------

from __future__ import absolute_import
from future import standard_library
standard_library.install_aliases()
from builtins import str
from builtins import object
import urllib.request, urllib.parse, urllib.error
import requests
import os
import mimetypes
from urllib.request import build_opener, HTTPCookieProcessor
import http.cookiejar
import sys
import urllib3

from elisa_client_api.exception import RestServerError


class Request(object):
    """ Encapsulates the functionality to perform HTTP requests.
    """
    def __init__(self, url, authentication):
        """ Constructor
            
        url: URL to make the request to. 
        """
        self.__url = url
        self.__authentication = authentication
        urllib3.disable_warnings()

    # -----------------------------
    # - Public methods: Interface -
    # -----------------------------
    def get(self):
        """ Makes a get request with the params in the argument.
            
        Returns: the data returned by the server.
        Throws: RestServerError if the request fails.
        """
        session = requests.Session()
        headers = {"Accept":"application/xml"}
        request = requests.Request('GET', self.__url, headers)
        prepped = request.prepare()
        try:
            self.__authentication.addAuthentication(prepped)
            #print(prepped.headers)
            response = session.send(prepped, verify=False)
            self._checkSsoAuthen(response)
            self._checkToken(response)
            #print(response.text)
            #for key, val in response.headers.items():
            #    print(key, ':', val)
            return response.content
        except requests.exceptions.HTTPError as ex:
            raise RestServerError(str(ex) + ". REST server error: " + ex.read().decode())

        
    def post(self, message):
        """ Makes a post request to insert a message.
            
        message: message to insert/post.
        Returns: the data returned by the server.
        Throws: RestServerError if the request fails.
        """
        session = requests.Session()
        headers = {"Accept":"application/xml", "Content-Type": "application/xml"}
        request = requests.Request('POST', self.__url, headers, data=message)
        prepped = request.prepare()
        try:
            self.__authentication.addAuthentication(prepped)
            #print(prepped.headers)
            response = session.send(prepped, verify=False)
            self._checkSsoAuthen(response)
            self._checkToken(response)
            #print(response.text)
            #for key, val in response.headers.items():
            #    print(key, ':', val)
            return response.content
        except requests.exceptions.HTTPError as ex:
            raise RestServerError(str(ex) + ". REST server error: " + ex.read().decode())
        
        
    def put(self, message):
        """ Makes a put request to update a message.
            
        message: message to update/post.
        Returns: the data returned by the server.
        Throws: RestServerError if the request fails.
        """
        session = requests.Session()
        headers = {"Accept":"application/xml", "Content-Type": "application/xml"}
        request = requests.Request('PUT', self.__url, headers, data=message)
        prepped = request.prepare()
        try:
            self.__authentication.addAuthentication(prepped)
            response = session.send(prepped, verify=False)
            self._checkSsoAuthen(response)
            self._checkToken(response)
            return response.content
        except requests.exceptions.HTTPError as ex:
            raise RestServerError(str(ex) + ". REST server error: " + ex.read().decode())
        
        
    def multipart(self, message=None, attachments=None):
        """ Makes a multipart request to insert a message and/or attachments.
            
        message: a tuple containing the message to insert/post and the 
                 content disposition name.
        attachments: attachments to insert.
        Returns: the data returned by the server.
        Throws: RestServerError if the request fails.
                FileError if any of the attachment cannot be opened.
        """
        session = requests.Session()
        files={}        
        if message != None:
            files[message[1]] = (None,message[0],'application/xml')
        count=0
        for attachment in attachments:
            files['file'+str(count)] = (os.path.basename(attachment), open(attachment, 'rb'), mimetypes.guess_type(attachment)[0] or 'application/octet-stream')
            count+=1
        request = requests.Request('POST', self.__url, files=files)
        prepped = session.prepare_request(request)
        self.__authentication.addAuthentication(prepped)
        #print(prepped.body.decode('ascii'), prepped.headers)
        
        try:
            response = session.send(prepped, verify=False)
            self._checkSsoAuthen(response)
            self._checkToken(response)
            #print(response.text)
            return response.content
        except requests.exceptions.HTTPError as ex:
            raise RestServerError(str(ex) + ". REST server error: " + ex.read().decode())
        #except urllib.error.URLError as ex:
        #    raise RestServerError(str(ex))
        
    # -------------------    
    # - Private methods -
    # -------------------
    def _checkSsoAuthen(self, response):
        # Is there a better way to detect this?
        if '<!DOCTYPE html PUBLIC' in response.text and 'Sign in with your CERN account' in response.text:
            raise RestServerError("SSO authentication failed.");()

    def _checkToken(self, response):
        # Is there a better way to detect this?
        if 'WWW-Authenticate' in response.headers and 'Bearer error' in response.headers['WWW-Authenticate']:
            #print(response.headers)
            raise RestServerError("Token authorization failed. " + response.headers['WWW-Authenticate']);()

