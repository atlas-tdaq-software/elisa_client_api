#include <string>
#include <vector>
#include <unistd.h>
#include <error.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <ers/ers.h>
#include <boost/algorithm/string.hpp>
#include "elisa_client_api/c++/elisa_client_api.h"

#define READ 0 // Read end of pipe 
#define WRITE 1 // Write end of pipe 

int elisa::ELisA_clientAPI::elisa_get(std::vector<std::string> input_params )
	//throw (elisa::FailToRun, elisa::ErrorFromREST)
{
  ERS_INFO("Get entries from ELisA.");
  //add program name to arguments list as is required by execvp
  std::vector<char *> argvExtend = transform_input(input_params, "elisa_get");
 
  //create pipes, fork child and read stdout
  create_child_read_pipes("elisa_get",argvExtend.data());
  return(EXIT_SUCCESS);
}

int elisa::ELisA_clientAPI::elisa_insert(std::vector<std::string> input_params)
	//throw (elisa::FailToRun, elisa::ErrorFromREST)
{
  ERS_INFO("Insert message to ELisA.");
  //add program name to arguments list as is required by execvp
  std::vector<char *> argvExtend = transform_input(input_params, "elisa_insert");
  
  //create pipes, fork child and read stdout
  create_child_read_pipes("elisa_insert",argvExtend.data());
  return(EXIT_SUCCESS);
}

std::map<std::string,std::vector<std::string>> elisa::ELisA_clientAPI::elisa_get_config(std::vector<std::string> input_params)
	//throw (elisa::FailToRun, elisa::ErrorFromREST)
{
  ERS_INFO("Get message configuration from ELisA.");
	
  //add program name to arguments list as is required by execvp
  std::vector<char *> argvExtend = transform_input(input_params, "elisa_config");

  //create pipes, fork child and read stdout
  create_child_read_pipes("elisa_config",argvExtend.data());

  return configM;

}

void elisa::ELisA_clientAPI::create_child_read_pipes(char* firstArg, char * argvExtend[])
{
  int aStdinPipe[2];
  int aStdoutPipe[2];
  int nChild;
  char buffer[4096];

  if (pipe(aStdinPipe) < 0) {
    perror("allocating pipe for child input redirect");
    throw elisa::FailToRun(ERS_HERE, "allocating pipe for child input redirect");
  }
  if (pipe(aStdoutPipe) < 0) {
    close(aStdinPipe[READ]);
    close(aStdinPipe[WRITE]);
    perror("allocating pipe for child output redirect");
    throw elisa::FailToRun(ERS_HERE, "allocating pipe for child output redirect");
  }

  nChild = fork();
  if (0 == nChild) {
    // child continues here
    // redirect stdin, stdout and stderr
    if (dup2(aStdinPipe[READ], STDIN_FILENO) == -1 ||
        dup2(aStdoutPipe[WRITE], STDOUT_FILENO) == -1 ||
        dup2(aStdoutPipe[WRITE], STDERR_FILENO) == -1
        ) 
    {
        throw elisa::FailToRun(ERS_HERE, "redirecting pipes failed.");
    }
    // all these are for use by parent only
    close(aStdinPipe[READ]);
    close(aStdinPipe[WRITE]);
    close(aStdoutPipe[READ]);
    close(aStdoutPipe[WRITE]); 

    // run child process
    execvp(firstArg, argvExtend);

    // if we get here at all, an error occurred, but we are in the child
    // process, so just exit
    perror("exec of the child process");
    throw elisa::FailToRun(ERS_HERE, "exec of the child failed.");
  } else if (nChild > 0) {
    // parent continues here

    // close unused file descriptors, these are for child only
    close(aStdinPipe[READ]);
    close(aStdoutPipe[WRITE]); 
    close(aStdinPipe[WRITE]);
   //read from stdout 
    while (1) {
	ssize_t count = read(aStdoutPipe[READ], buffer, sizeof(buffer));
	if (count == -1) {
	if (errno == EINTR) {
	      continue;
	} else {
	      perror("read");
	      throw elisa::FailToRun(ERS_HERE, "Can not read from pipe.");
	}
	} else if (count == 0) {
	    break;
	} else {
	    configM = handle_child_process_output(buffer, count);
	}
    }
    // finished with these, should keep these
    // as long as you want to talk to the child
    close(aStdoutPipe[READ]);
  } else {
    // failed to create child
    close(aStdinPipe[READ]);
    close(aStdinPipe[WRITE]);
    close(aStdoutPipe[READ]);
    close(aStdoutPipe[WRITE]);
    throw elisa::FailToRun(ERS_HERE, "fork failed.");
  }


}
std::map<std::string,std::vector<std::string>> elisa::ELisA_clientAPI::handle_child_process_output(char buffer[], ssize_t count)
{
    std::map<std::string,std::vector<std::string>> configMap;
    std::string str(buffer);
    if(str.find("<module>:40 [ERROR]")!=std::string::npos) {
      	throw elisa::ErrorFromREST(ERS_HERE,buffer);
    }
    else if(str.find("error: missing mandatory argument")!=std::string::npos || str.find("error: invalid value format of argument")!=std::string::npos) {
      	throw elisa::FailToRun(ERS_HERE,buffer);
    }
    else {
	//write(STDOUT_FILENO, &buffer, sizeof(buffer));
        std::cout<<str<<std::endl;
	
	std::string mtinit="List of possible types:";
	std::size_t startI=str.find(mtinit);
	if(startI!=std::string::npos){
	  startI+=mtinit.length()+2;
	  std::size_t endI=str.find("]",startI);
	  std::string mt=str.substr(startI,endI-startI);
	  std::cout<<"mt"<<mt<<std::endl;
	  std::vector<std::string> mts;
          boost::split(mts, mt, boost::is_any_of(","), boost::token_compress_on);
	  configMap.insert(std::make_pair("MT",mts));
	  //for(std::vector<std::string>::iterator vi=mts.begin(); vi!=mts.end();++vi)
		//std::cout<<*vi<<std::endl;
	}
	
	//std::cout<<"Initial str"<<str<<std::endl;
	std::string sainit="List of possible systems affected:";
	std::size_t startII=str.find(sainit);
	if(startII!=std::string::npos){
	  startII+=sainit.length()+2;
	  std::size_t endII=str.find("]",startII);
	  std::string sa=str.substr(startII,endII-startII);
	  std::cout<<"sa"<<sa<<std::endl;
	  std::vector<std::string> sas;
          boost::split(sas, sa, boost::is_any_of(","), boost::token_compress_on);
          configMap.insert(std::make_pair("SA",sas));
	  //for(std::vector<std::string>::iterator vi=sas.begin(); vi!=sas.end();++vi)
		//std::cout<<*vi<<std::endl;
	}

	std::string optinit="Valid options for ";
	std::string inoptinit="Inner options:";
	std::size_t startIV=str.find(optinit);
	if(startIV!=std::string::npos){
	  std::size_t y=str.find(":",startIV);
	  std::size_t dd = y-startIV;
	  startIV+=dd+1;
	  std::size_t endIV = str.find(inoptinit,startIV);
	  std::string opt=str.substr(startIV,endIV-startIV);
	  std::cout<<"opt "<<opt<<std::endl;
	  std::vector<std::string> opts;
	  opts.push_back(opt);
          //boost::split(opts, opt, boost::is_any_of(":"), boost::token_compress_on);
          configMap.insert(std::make_pair("OPTIONS",opts));
	  //for(std::vector<std::string>::iterator vi=opts.begin(); vi!=opts.end();++vi)
		//std::cout<<*vi<<std::endl;
	
	  if(endIV!=std::string::npos) {
			  	
		std::size_t ii=str.find("Valid systems affected for",endIV);
		endIV+=inoptinit.length();		
		std::string inopt=str.substr(endIV,ii-endIV);
		std::cout<<"inopt "<<inopt<<std::endl;
		std::vector<std::string> inopts;
		inopts.push_back(inopt);
         	configMap.insert(std::make_pair("INNER-OPTIONS",inopts));
	  }	
	}

	std::string sadefinit="Valid systems affected for ";
	std::size_t startIII=str.find(sadefinit);
	if(startIII!=std::string::npos){
	  std::size_t y=str.find(":",startIII);
	  std::size_t dd = y-startIII;
	  startIII+=dd+3;
	  std::size_t endIII=str.find("]",startIII);
	  if(endIII!=std::string::npos) {
	  	std::string sadef=str.substr(startIII,endIII-startIII);
	  	std::cout<<"sadef "<<sadef<<std::endl;
	  	std::vector<std::string> sasdef;
          	boost::split(sasdef, sadef, boost::is_any_of(","), boost::token_compress_on);
          	configMap.insert(std::make_pair("SA-predefined",sasdef));
	  	for(std::vector<std::string>::iterator vi=sasdef.begin(); vi!=sasdef.end();++vi)
			std::cout<<*vi<<std::endl;
	  }
	}
	   	
    }
    return configMap;   
}

std::vector<char *> elisa::ELisA_clientAPI::transform_input(std::vector<std::string> & input_params, std::string elem_to_add)
{
    //Add first element as the name of the script needed by execv
    input_params.insert(input_params.begin(),elem_to_add);
    //convert back to char * array for the execvp usage    
    std::vector<char *> argvExtend(input_params.size() + 1);    // one extra for the null
    for(std::size_t i = 0; i != input_params.size(); ++i)
    {
       argvExtend[i] = const_cast<char*>(input_params[i].c_str());
    }
    return argvExtend;
}
