/**
 *
 * \brief Test application for C++ wrapper for ELisA client API.
 *
 * This is a test application to show how to use C++ wrapper for ELisA client API
 * to retrieve messages from ELisA logbook. 
 *
 * \author $Author: Alina Corso-Radu (jan. 2014)  $
 *
 * \date $Date: Jan. 2014 $
 *
 * Contact: alina.radu@cern.ch
 *
 * Created on: 09.01.2014
 *
 *
 */
#include <ers/ers.h>
#include "elisa_client_api/c++/elisa_client_api.h"
#include "elisa_client_api/c++/exceptions.h"
int main(int argc, char *argv[])
{	
  elisa::ELisA_clientAPI * elisa_client;
  try {
     elisa_client = new elisa::ELisA_clientAPI();
  } catch( ers::Issue & ex ) {
     ers::error( ex );
  }
  //convert char *argv[] into std::string
  std::vector<std::string> input_params;
  for (int i = 1; i < argc; ++i) {
     input_params.push_back(argv[i]);
  }
  try {
     elisa_client->elisa_get(input_params);
  } catch(ers::Issue & ex) {
     ers::error( ex );
  }

  std::cout<<"after call to elisa_get"<<std::endl;

}
