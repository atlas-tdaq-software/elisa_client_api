package elisa_client.service;

/**
 * Custom exception
 */
public class ElisaApiException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ElisaApiException(String msg) {
        super(msg);
    }
}

