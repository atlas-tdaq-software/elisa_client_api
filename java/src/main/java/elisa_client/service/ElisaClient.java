package elisa_client.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.Map;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import elisa_client.model.Attachment;
import elisa_client.model.AttachmentList;
import elisa_client.model.InputMessage;
import elisa_client.model.Message;
import elisa_client.model.MessageBody;
import elisa_client.model.MessageList;
import elisa_client.model.MessageType;
import elisa_client.model.MessageTypeList;
import elisa_client.model.OptionMetadata;
import elisa_client.model.OptionMetadataList;
import elisa_client.model.SystemAffectedList;

import daq.eformat.DetectorMask;
import daq.eformat.DetectorMask.DETECTOR;


/**
 * Client used for communicating with the ELisA REST web service
 * @author alina
 *
 */
@Component( "elisaClient" )
public class ElisaClient
{
    private static final Log log = LogFactory.getLog(ElisaClient.class.getName());
	/**
	 * Facilitates communication with the elisa rest; autowired by Spring
	 */
	@Autowired
	private RestTemplate restTemplate;
	
	/**
	 * The base URL of the elisa rest web service - it is configurable via runtime.properties file
	 */
	@Value("${elisaRest.URL}")
	private String elisarestUrl;

	/**
	 * create Authorization header using:
	 * 1.--for LDAP -- Basic Authentication: authorization header containing encoded user credentials.
	 * 2.--for SSO --- Cookie: authorization header containing info from sso cookie file created previously using auth-get-sso-cookie 
	 * @param username
	 * @param password
	 * @param ssocookiefile
	 * @return Authorization header
	 */
	MultiValueMap<String,String> createAuthHeaders( final String username, final String pwd, final String ssocookiefile )
	{
		MultiValueMap<String,String> headers = new LinkedMultiValueMap<String,String>();
	        if(username==null) {
        		String ssocookie=null;
    	    		File file = new File(ssocookiefile);
    	    		Scanner scanner = null;
			try {
				scanner = new Scanner(file);
			} catch (FileNotFoundException e) {
				throw new ElisaApiException(e.getMessage());
			}
    	    
    	    ArrayList<String> words = new ArrayList<String>();    	    	 
    	    String theWord; 
    	    try{
	    	    while (scanner.hasNext()){
	    	    theWord = scanner.next();
	    	    words.add(theWord);
	    	    }
    	    } finally {
    	        scanner.close();
    	    }   	    
        	//if word in file contains _shibsession_ take the next one after and build the cookie 
    	    for(int i=0;i<words.size();i++){
    	    	//System.out.println(words.get(i));
    	    	if(words.get(i).contains("_shibsession_")){
    	    		//System.out.println("WHAT I NEED "+words.get(i+1));
    	    		ssocookie=words.get(i)+"="+words.get(i+1);
    	    		break;
    	    	}
    	    }
        	headers.add("Cookie", ssocookie);
        }
        else{
		String auth = username + ":" + pwd;
	        byte[] encodedAuth = Base64.encodeBase64( auth.getBytes() );
	        String authHeader = "Basic " + new String( encodedAuth );
	        headers.add( "Authorization", authHeader );
        }
		return headers;
	}

	/**
	 * Return the entry with a given ID from the ELisA REST server
	 * @param username
	 * @param pwd
	 * @param ssocookiefile: path to file containing the sso cookie
	 * @param msgId: the message ID to be retrieved
	 * @return Object of type Message (class imported from ELisA REST server) 
	 * @throws Exception 
	 */
	public Message getMessage( String username, String pwd, String ssocookiefile, long msgId)
	{
 		//System.out.println("my getMessage");
        	return getMessage( username, pwd, ssocookiefile, "ATLAS", msgId); 	
	}

	/**
	 * Return the entry with a given ID from the ELisA REST server
	 * @param username
	 * @param pwd
	 * @param ssocookiefile: path to file containing the sso cookie
	 * @param msgId: the message ID to be retrieved
	 * @param logbook: logbookname to be used to retrieve the message from
	 * @return Object of type Message (class imported from ELisA REST server) 
	 * @throws Exception 
	 */
	public Message getMessage( String username, String pwd, String ssocookiefile, String logbook, long msgId)
	{
		//this is to deal with the SSL connection.
		HttpClient httpClient = HttpUtils.getNewHttpClient();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

		ResponseEntity<Message> response=null;
		//create the authentication header for the request
		MultiValueMap<String,String> headers = createAuthHeaders(username,pwd,ssocookiefile);
        //headers.add("Accept", "text/html;charset=utf-8");
        //headers.add("Content-Type","text/html");
	//headers.add("Content-Type","application/json");
	//	Set keys = headers.keySet();  
	//	for (Object k : keys) {  
	//	    System.out.println(k+": "+headers.get(k));
	//	} 
	
		HttpEntity<Object> requestEntity=new HttpEntity<Object>(headers);

		try{
			response=restTemplate.exchange(elisarestUrl + logbook + "/messages/{id}",
					HttpMethod.GET,requestEntity,Message.class,msgId);
			return response.getBody();        	
		} catch (ElisaApiException exception) {
			throw exception;		
		} catch (Exception exception) {
			throw new ElisaApiException(exception.getMessage());
		}		
	}

	public Attachment getAttachment( String username, String pwd, long msgId, long attachId )
	{
		return null;
	}

	public AttachmentList getAttachments( String username, String pwd, long msgId )
	{
		return null;
	}

	public MessageList searchMessages( String username, String pwd )
	{
		return null;
	}

	/**
	 * Insert a logbook entry with given properties encapsulated into object of type InputMessage
	 * @param username
	 * @param pwd
	 * @param ssocookiefile: path to file containing the sso cookie
	 * @param msg: object of type InputMessage to be inserted into logbook
	 * @return object of type Message
	 * @throws Exception 
	 */
	public Message insertMessage( String username, String pwd, String ssocookiefile, InputMessage msg ) 
	{
		//System.out.println("my insertMessage");
		return insertMessage( username, pwd, ssocookiefile, "ATLAS", msg );
	}

	/**
	 * Insert a logbook entry with given properties encapsulated into object of type InputMessage
	 * @param username
	 * @param pwd
	 * @param ssocookiefile: path to file containing the sso cookie
	 * @param msg: object of type InputMessage to be inserted into logbook
	 * @param logbook: logbook name to be used to insert the message
	 * @return object of type Message
	 * @throws Exception 
	 */
	public Message insertMessage( String username, String pwd, String ssocookiefile, String logbook, InputMessage msg ) 
	{				
		//this is to deal with the SSL connection.
		HttpClient httpClient = HttpUtils.getNewHttpClient();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

		ResponseEntity<Message> response = null;
		//create the authentication header for the request
		MultiValueMap<String,String> headers = createAuthHeaders(username,pwd,ssocookiefile);
        //set the body of the request as the InputMessage        
		HttpEntity<Object> requestEntity=new HttpEntity<Object>(msg,headers);
        try{
        	response=restTemplate.exchange(elisarestUrl + logbook +"/messages",
				HttpMethod.POST,requestEntity,Message.class);
        	return response.getBody();        	
	    } catch (ElisaApiException exception) {
	    	throw exception;
	    } catch (Exception exception) {
			throw new ElisaApiException(exception.getMessage());
		}		
				
	}
	
	public Message updateMessage( String username, String pwd, MessageBody update )
	{
		return null;
	}

	public Message replyToMessage( String username, String pwd )
	{
		return null;
	}

	/**
     	* Reply to a logbook entry ID with given properties encapsulated into object of type InputMessage
     	*
     	* @param username
     	* @param pwd
     	* @param ssocookiefile: path to file containing the sso cookie
     	* @param msg:           object of type InputMessage to be inserted into logbook
     	* @return object of type Message
     	* @throws Exception
     	*/
    	public Message replyToMessage(String username, String pwd, String ssocookiefile, long replyToID, InputMessage msg) {

        	return replyToMessage(username, pwd, ssocookiefile, "ATLAS", replyToID, msg);
    	}

    	/**
     	* Reply to a logbook entry ID with given properties encapsulated into object of type InputMessage
     	*
     	* @param username
     	* @param pwd
     	* @param ssocookiefile: path to file containing the sso cookie
     	* @param logbook:       logbookname to be used to retrieve the message from
     	* @param msg:           object of type InputMessage to be inserted into logbook
     	* @return object of type Message
     	* @throws Exception
     	*/
    	public Message replyToMessage(String username, String pwd, String ssocookiefile, String logbook, long replyToID, InputMessage msg) {
        	//this is to deal with the SSL connection.
        	HttpClient httpClient = HttpUtils.getNewHttpClient();
        	restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
        	ResponseEntity<Message> response = null;
        	//create the authentication header for the request
        	MultiValueMap<String, String> headers = createAuthHeaders(username, pwd, ssocookiefile);
        	//set the body of the request as the InputMessage
       		HttpEntity<Object> requestEntity = new HttpEntity<Object>(msg, headers);
        	try {
            		response = restTemplate.exchange(elisarestUrl + logbook + "/messages/" + replyToID, HttpMethod.POST, requestEntity, Message.class);
            		return response.getBody();
        	} catch (ElisaApiException exception) {
            		throw exception;
        	} catch (Exception exception) {
            		throw new ElisaApiException(exception.getMessage());
        	}
    	}


	/**
	 * Get from logbook configuration the list of possible message types
	 * Method that can be used without user credentials
	 * 
	 * @param username
	 * @param pwd
	 * @param ssocookiefile: path to file containing the sso cookie
	 * 
	 * @return a list of message types
	 * @throws ElisaApiException 
	 */
	public MessageTypeList getMessageTypes( String username, String pwd, String ssocookiefile)
	{
		//this is to deal with the SSL connection.
		HttpClient httpClient = HttpUtils.getNewHttpClient();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

		//the type of expected response
		ResponseEntity<MessageTypeList> response=null;				
		//if there is no user credentials, get configuration without authentication (for IGUI)
		if(username==null && ssocookiefile==null) {
			try{  		
				response=restTemplate.getForEntity(elisarestUrl + "mt",MessageTypeList.class);
				return response.getBody();  
			} catch (ElisaApiException exception) {
				throw exception;
			} catch (Exception exception) {
				throw new ElisaApiException(exception.getMessage());
			}		

		}
		else {
			//create the authentication header for the request
			MultiValueMap<String,String> headers = createAuthHeaders(username,pwd,ssocookiefile);
			HttpEntity<Object> requestEntity=new HttpEntity<Object>(headers);
			try{
				response=restTemplate.exchange(elisarestUrl + "mt",
						HttpMethod.GET,requestEntity,MessageTypeList.class);
				return response.getBody();  
			} catch (ElisaApiException exception) {
				throw exception;
			} catch (Exception exception) {
				throw new ElisaApiException(exception.getMessage());
			}		
			
		}
	}

	/**
	 * Get from logbook configuration the options for a specified message type
	 * Method that can be used without user credentials
	 *
	 * @param username
	 * @param pwd
	 * @param ssocookiefile: path to file containing the sso cookie
	 * @param msgType: a given message type
	 * @return the options of that message type 
	 * @throws Exception 
	 */
	public OptionMetadataList getMessageTypeOptions( String username, String pwd, String ssocookiefile, String msgType )
	{
		//this is to deal with the SSL connection.
		HttpClient httpClient = HttpUtils.getNewHttpClient();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

		//the type of expected response
		ResponseEntity<OptionMetadataList> response=null;

		//if there is no user credentials, get configuration without authentication (for IGUI)
		if(username==null && ssocookiefile==null) {
			try{
				response=restTemplate.getForEntity(elisarestUrl + "mt/"+msgType+"/opt",OptionMetadataList.class);
				return response.getBody(); 

			} catch (ElisaApiException exception) {
				throw exception;
			} catch (Exception exception) {
				throw new ElisaApiException(exception.getMessage());
			}		

		}
		else {
			//create the authentication header for the request
			MultiValueMap<String,String> headers = createAuthHeaders(username,pwd,ssocookiefile);
			HttpEntity<Object> requestEntity=new HttpEntity<Object>(headers);
			try{
				response=restTemplate.exchange(elisarestUrl + "mt/"+msgType+"/opt",
						HttpMethod.GET,requestEntity,OptionMetadataList.class);
				return response.getBody(); 

			} catch (ElisaApiException exception) {
				throw exception;
			} catch (Exception exception) {
				throw new ElisaApiException(exception.getMessage());
			}		
			
		}
	}
	
	/**
	 * Get from logbook configuration the preset SA for a specified message type
	 * Method that can be used without user credentials
	 * @param username
	 * @param pwd
	 * @param ssocookiefile: path to file containing the sso cookie
	 * @param msgType: a given message type; if null return the list of all possible systems affected
	 * @return a list of SA for that message type or a list of all possible SA 
	 * @throws Exception
	 */
	public SystemAffectedList getSystemsAffected( String username, String pwd, String ssocookiefile, String msgType )
	{
		//this is to deal with the SSL connection.
		HttpClient httpClient = HttpUtils.getNewHttpClient();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

		//the type of expected response
		ResponseEntity<SystemAffectedList> response=null;

		//if there is no user credentials, get configuration without authentication (for IGUI)
		if(username==null && ssocookiefile==null) {
			if(msgType==null){
				try{
					response=restTemplate.getForEntity(elisarestUrl + "sa",SystemAffectedList.class);
					return response.getBody();
				} catch (ElisaApiException exception) {
					throw exception;
				} catch (Exception exception) {
					throw new ElisaApiException(exception.getMessage());
				}

			}
			else {
				try{
					response=restTemplate.getForEntity(elisarestUrl + "mt/"+msgType+"/sa",SystemAffectedList.class);
					return response.getBody();
				} catch (ElisaApiException exception) {
					throw exception;
				} catch (Exception exception) {
					throw new ElisaApiException(exception.getMessage());
				}
			}
		}
		else {
			//create the authentication header for the request
			MultiValueMap<String,String> headers = createAuthHeaders(username,pwd,ssocookiefile);
			HttpEntity<Object> requestEntity=new HttpEntity<Object>(headers);

			if(msgType==null){
				try{
					response=restTemplate.exchange(elisarestUrl + "sa",
							HttpMethod.GET,requestEntity,SystemAffectedList.class);
					return response.getBody();

				} catch (ElisaApiException exception) {
					throw exception;
				} catch (Exception exception) {
					throw new ElisaApiException(exception.getMessage());
				}

			}
			else {
				try{
					response=restTemplate.exchange(elisarestUrl + "mt/"+msgType+"/sa",
							HttpMethod.GET,requestEntity,SystemAffectedList.class);
					return response.getBody();

				} catch (ElisaApiException exception) {
					throw exception;
				} catch (Exception exception) {
					throw new ElisaApiException(exception.getMessage());
				}
			}
		}
	}

	/*
	 * Debug method
	 * return plain xml 
	 */
	public String getMessageTypeOptionsxml( String username, String pwd, String ssocookiefile, String msgType )
	{
		//this is to deal with the SSL connection.
		HttpClient httpClient = HttpUtils.getNewHttpClient();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

		//the type of expected response
		ResponseEntity<String> response=null;
		
		//create the authentication header for the request
		MultiValueMap<String,String> headers = createAuthHeaders(username,pwd,ssocookiefile);
		HttpEntity<Object> requestEntity=new HttpEntity<Object>(headers);
        	try{
    		response=restTemplate.exchange(elisarestUrl + "mt/"+msgType+"/opt.xml",
    					HttpMethod.GET,requestEntity,String.class);
        	return response.getBody(); 
  
 	    } catch (ElisaApiException exception) {
	    	throw exception;
	    } catch (Exception exception) {
			throw new ElisaApiException(exception.getMessage());
		}		

	}

	/**
	 * * Get from configuration db the corresponding SA per detector. Used by IGUI
	 * Method can be used without credentials
	 * @param username
	 * @param pwd
	 * @param ssocookiefile
	 * @param d : eformat:DetectorMask:DETECTOR
	 * @return string system affected name or empty if that SA does not exist
	 */
	public String getSAforDetector(String username, String pwd, String ssocookiefile, DETECTOR d)
	{
		/* From IGUI code
		* nothing specific is done except for detector masks MICROMEGA (SA=MMG) and DAQ (SA=Central DAQ)
		* To be checked when new detector masks are added or if SA names are changing!!
		* */
        /*this.detMap.put(SystemAffected.CENTRAL_DAQ.toString(), DETECTOR.DAQ);
        this.detMap.put(SystemAffected.PIXEL.toString(), DETECTOR.PIXEL);
        this.detMap.put(SystemAffected.SCT.toString(), DETECTOR.SCT);
        this.detMap.put(SystemAffected.TRT.toString(), DETECTOR.TRT);
        this.detMap.put(SystemAffected.LARG.toString(), DETECTOR.LAR);
        this.detMap.put(SystemAffected.TILE.toString(), DETECTOR.TILE);
        this.detMap.put(SystemAffected.MDT.toString(), DETECTOR.MDT);
        this.detMap.put(SystemAffected.RPC.toString(), DETECTOR.RPC);
        this.detMap.put(SystemAffected.TGC.toString(), DETECTOR.TGC);
        this.detMap.put(SystemAffected.CSC.toString(), DETECTOR.CSC);
        this.detMap.put(SystemAffected.LVL1.toString(), DETECTOR.LVL1);
        this.detMap.put(SystemAffected.BCM.toString(), DETECTOR.BCM);
        this.detMap.put(SystemAffected.LUCID.toString(), DETECTOR.LUCID);
        this.detMap.put(SystemAffected.ZDC.toString(), DETECTOR.ZDC);
        this.detMap.put(SystemAffected.ALFA_RPO.toString(), DETECTOR.ALFA);
        this.detMap.put(SystemAffected.HLT.toString(), DETECTOR.HLT);
        this.detMap.put(SystemAffected.MMG.toString(), DETECTOR.MICROMEGA);
        this.detMap.put(SystemAffected.AFP.toString(), DETECTOR.AFP);
        this.detMap.put(SystemAffected.STGC.toString(), DETECTOR.STGC);*/

		SystemAffectedList saTypes;
		String matchingSA="";
		try {
			saTypes = getSystemsAffected(null, null, null, null);
			for (int i = 0; i < saTypes.getList().size(); i++) {
				matchingSA = saTypes.getList().get(i);
				String tempDetName=d.toString();
				if(d.toString().equals("MICROMEGA")){
					tempDetName="MMG";
				}
				else if(d.toString().equals("DAQ")) {
					tempDetName="Central DAQ";
				}
				else if(d.toString().equals("STGC")) {
					tempDetName="STG";
				}
				if (matchingSA.toLowerCase().contains(tempDetName.toLowerCase())) {
					break;
				} else {
					//System.out.println("matchingSA:"+matchingSA);
					matchingSA="";
				}
			}
		} catch (ElisaApiException e) {
			System.out.println(e.getMessage());
		}
		return matchingSA;
	}

	/**
	 * Get from the configuration db the message type to be used by Igui
	 * Assume this is MT=Online
	 * @param username
	 * @param pwd
	 * @param ssocookiefile
	 * @return string with MT name, empty if not found
	 */
	public String getMTforIGUI(String username, String pwd, String ssocookiefile)
	{

		MessageTypeList msgTypes;
		String onlMT="";
		try {
			msgTypes = getMessageTypes(null,null,null);
			for(int i=0; i<msgTypes.getList().size();i++){
				onlMT = msgTypes.getList().get(i);
				if (onlMT.contains("Online")) {
					break;
				}
			}
		} catch (ElisaApiException e) {
			System.out.println(e.getMessage());
		}
		return onlMT;
	}
}
