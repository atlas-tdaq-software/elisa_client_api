package elisa_client.service;

import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

public class ElisaResponseErrorHandler implements ResponseErrorHandler {

	private static final Log log = LogFactory.getLog(ElisaResponseErrorHandler.class.getName());
	private ResponseErrorHandler errorHandler = new DefaultResponseErrorHandler();

	//There are two methods which need to be implemented: hasError and handleError. 
	//If the hasError method returns true then Spring will automatically call the handleError method. 
	 
	@Override
	public boolean hasError(ClientHttpResponse clienthttpresponse) throws IOException {	 
		if (  clienthttpresponse.getStatusCode() != HttpStatus.OK 
		   && clienthttpresponse.getStatusCode() != HttpStatus.CREATED ) {
			//log.debug("Status code: " + clienthttpresponse.getStatusCode());
			//log.debug("Response: " + clienthttpresponse.getStatusText());
			//log.debug("Body: "+ clienthttpresponse.getBody());
			/*byte[] b = new byte[512];
		    StringBuffer sb = new StringBuffer();
		    while (clienthttpresponse.getBody().read(b) != -1) {
		       sb.append(new String(b, "utf-8"));
		       b = new byte[512];
		    }   
			log.debug("Body string: "+ sb.toString());*/
			return true;
		}
		else
			return false;
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		log.debug("Status code: " + response.getStatusCode());
		log.debug("Response: " + response.getStatusText());
		//log.debug("Body: "+ response.getBody());
		if(response.getStatusCode() != HttpStatus.NOT_FOUND) {
			byte[] b = new byte[512];
			StringBuffer sb = new StringBuffer();
			while (response.getBody().read(b) != -1) {
				sb.append(new String(b, "utf-8"));
				b = new byte[512];
			}   
			log.debug("Body string: "+ sb.toString());
			try {           
				errorHandler.handleError(response);
			} catch (RestClientException scx) {         
				throw new ElisaApiException( sb.toString() );
			}
		}
		else{
			throw new ElisaApiException( "No resource found." );
		}
	}
		
}
