package elisa_client.model;

/**
 * 
 * Message Type 
 *
 * @author lucamag
 *
 */
public class MessageType {
	private final String mt_name;  
	private MessageType(String mt_name) {  
		this.mt_name = mt_name;  
	}  
	@Override  
	public String toString() {  
		return mt_name;  
	}  
}
