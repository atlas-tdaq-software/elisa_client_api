package elisa_client.model;

/**
 * 
 * System Affected 
 * 
 * 
 * @author 
 *
 */
public class SystemAffected {
	
	private final String sa_name;  
	private SystemAffected(String sa_name) {  
		this.sa_name = sa_name;  
	}  
	@Override  
	public String toString() {  
		return sa_name;  
	}  
}
