package elisa_client.model;

import java.util.List;

import javax.xml.bind.annotation.*;

import org.apache.commons.lang.StringUtils;

@XmlRootElement(name="systems_affected")
//@XmlType
@XmlAccessorType(XmlAccessType.FIELD)

public class SystemAffectedList {
	
	private int count;

	@XmlElement(name="system_affected")
	private List<String> sa;

	public SystemAffectedList() {}
	
	public SystemAffectedList(List<String> l) {
		this.sa = l;
		this.count = l.size();
	}

	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	public List<String> getList() {
		return sa;
	}
	public void setList(List<String> l) {
		this.sa = l;
		this.count = sa.size();
	}
	
	public String getListString() {
	    return StringUtils.join(this.sa,  " | ");
	}

}
