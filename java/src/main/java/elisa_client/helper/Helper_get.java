package elisa_client.helper;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import elisa_client.model.Message;
import elisa_client.model.MessageTypeList;
import elisa_client.model.OptionMetadataList;
import elisa_client.service.ElisaApiException;
import elisa_client.service.ElisaClient;
import elisa_client.model.SystemAffectedList;
import daq.eformat.DetectorMask; 
import daq.eformat.DetectorMask.DETECTOR;

public class Helper_get
{
	public static void main( String[] args )
	{
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"java/src/main/resources/applicationContext.xml" );
		//ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"applicationContext.xml" );
		ElisaClient elisaClient = applicationContext.getBean( "elisaClient", ElisaClient.class );
		
		if (elisaClient != null) {

			//How to retrieve a message by ID
			
			/*Message msgA;
			try {
				//msgA = elisaClient.getMessage("atlog","null",null,8);
				msgA = elisaClient.getMessage("elisaAPI","elisaAPIp",null,"TEST",81);
				//msgA = elisaClient.getMessage(null,null,"/home/alina/Desktop/cookie-pcatdelisa2",8);
				//msgA = elisaClient.getMessage(null,null,"/home/alina/Desktop/cookie-pcatdelisa2","ATLAS",134203);
				System.out.println( "getMessage Body: " + msgA.getBody() );
			} catch (ElisaApiException e) {
				System.out.println(e.getMessage());
			}*/

			//How to retrieve all possible message types 

			MessageTypeList msgTypes;
			try {
				msgTypes = elisaClient.getMessageTypes(null, null, null);
				//msgTypes = elisaClient.getMessageTypes(null,null,"/home/alina/Desktop/ssocookie.txt");
				System.out.println("Possible message types: ");
				for (int i = 0; i < msgTypes.getList().size(); i++) {
					System.out.println(msgTypes.getList().get(i));
				}
			} catch (ElisaApiException e) {
				System.out.println(e.getMessage());
			}

			SystemAffectedList saTypes;
			try {
				saTypes = elisaClient.getSystemsAffected(null, null, null, null);
				System.out.println("###List of systems affected: ");
				for (int i = 0; i < saTypes.getList().size(); i++) {
					System.out.println(saTypes.getList().get(i));
				}
			} catch (ElisaApiException e) {
				System.out.println(e.getMessage());
			}

			String saIgui;
			try {
				saIgui = elisaClient.getSAforDetector(null, null, null, DETECTOR.DAQ);
				System.out.println("### Matching SA for IGUI: " + saIgui);

			} catch (ElisaApiException e) {
				System.out.println(e.getMessage());
			}

			String mtIgui;
			try {
				mtIgui = elisaClient.getMTforIGUI(null, null, null);
				System.out.println("### MT for IGUI: " + mtIgui);

			} catch (ElisaApiException e) {
				System.out.println(e.getMessage());
			}

		} else {
			System.out.println("elisaClient bean is null");
		}

	}
}
