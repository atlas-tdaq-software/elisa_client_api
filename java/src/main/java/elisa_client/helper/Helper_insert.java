package elisa_client.helper;

import java.util.LinkedList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import elisa_client.model.InputMessage;
import elisa_client.model.Message;
import elisa_client.model.MessageType;
import elisa_client.model.Option;
import elisa_client.model.OptionList;
import elisa_client.model.SystemAffected;
import elisa_client.model.SystemAffectedList;
import elisa_client.service.ElisaClient;
import elisa_client.model.MessageTypeList;
import elisa_client.model.OptionMetadataList;
import elisa_client.service.ElisaApiException;

public class Helper_insert
{
	public static void main( String[] args )
	{
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"java/src/main/resources/applicationContext.xml" );
		ElisaClient elisaClient = applicationContext.getBean( "elisaClient", ElisaClient.class );
		
		if(elisaClient!=null){
		
		//get first list of possible message types
		
		MessageTypeList msgTypes;
		try {
			//Read logbook configuration with authentication
			//msgTypes = elisaClient.getMessageTypes("atlog","el0g_2013",null);
			//msgTypes = elisaClient.getMessageTypes(null,null,"/home/alina/Desktop/ssocookie.txt");
			
			//Read configuration with no authentication at all
			msgTypes = elisaClient.getMessageTypes(null,null,null);
			System.out.println( "Possible message types: ");
			for(int i=0; i<msgTypes.getList().size();i++){
				System.out.println(msgTypes.getList().get(i) );				
			}
		} catch (ElisaApiException e) {
			System.out.println(e.getMessage());
		}

		//Then get the list of options for a given message type
		OptionMetadataList opts;
		try {
			String mt="Online";
			//Read logbook configuration with user authentication
			//opts = elisaClient.getMessageTypeOptions("atlog","el0g_2013",null,mt);
			//opts = elisaClient.getMessageTypeOptions(null,null,"/home/alina/Desktop/ssocookie.txt",mt);
			
			//Read logbook configuration without authentication
			opts = elisaClient.getMessageTypeOptions(null,null,null,mt);
			System.out.println( "Options for message type "+mt);
			for(int i=0; i<opts.getList().size();i++){
				System.out.println(opts.getList().get(i).getName()+
								" "+opts.getList().get(i).getType()+
								" "+opts.getList().get(i).getPossibleValues());
				//System.out.println(opts.getList().get(i).getComment());
			}
		} catch (ElisaApiException e) {
			System.out.println(e.getMessage());
		}
		
		//Then get the list of preset SA for that MT and options
		SystemAffectedList psa;
		try {
			String mt="Online";
			System.out.println( "Get preset SA for message type: "+mt);
			//Read logbook configuration with user authentication
			//psa = elisaClient.getSystemsAffected("atlog","el0g_2013",null,mt);
			//psa = elisaClient.getSystemsAffected(null,null,"~/ssocookie.txt",mt);

			//Read logbook configuration without authentication
			psa = elisaClient.getSystemsAffected(null,null,null,mt);
			for(int i=0; i<psa.getList().size();i++){
				System.out.println(psa.getList().get(i));
			}
		} catch (ElisaApiException e) {
			System.out.println(e.getMessage());
		}
		
				
		//Then create and insert the logbook message
		
		InputMessage im = new InputMessage();
	        im.setBody("new msg from java client api");
	        im.setMessageType("Default Message Type");
	        im.setSubject("Client api java message");
	        im.setAuthor("Alina");
	        List<String> sa =  new LinkedList<String>();
	        sa.add("DAQ");
	        im.setSystemsAffected(new SystemAffectedList(sa));
	        im.setStatus("closed");        
	        //Option shsum = new Option();
	        //shsum.setName("ShiftSummary_Desk");
	        //shsum.setValue("DAQ");        
	        /*Option triggerG = new Option();
	        triggerG.setName("Trigger_Group");
	        triggerG.setValue("Calo");
	        Option triggerId = new Option();
	        triggerId.setName("Trigger_Group");
	        triggerId.setValue("ID");
	        List<Option> innerlist = new LinkedList<Option>();
	        innerlist.add(triggerG);
	        innerlist.add(triggerId);
	        triggerArea.setOptions(new OptionList(innerlist));*/
	        //List<Option> list = new LinkedList<Option>();
	        //list.add(shsum);
	        //im.setOptions(new OptionList(list));
	        		
	        Message msgB = null;
		try {
			//msgB = elisaClient.insertMessage("elisaAPI","bla",null, "TEST", im);
			//msgB = elisaClient.insertMessage(null,null,"/home/alina/Desktop/cookie-pcatdelisa2","TEST",im);
			msgB = elisaClient.insertMessage(null,null,"/home/alina/Desktop/cookie-pcatdelisa2",im);
			System.out.println( "insertMessage body: " + msgB.getBody() );
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}	
		}
		else{System.out.println("elisaClient bean is null");}

	}
 }
