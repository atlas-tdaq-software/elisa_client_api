package elisa_client;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import elisa_client.model.InputMessage;
import elisa_client.model.Message;
import elisa_client.model.MessageType;
import elisa_client.model.MessageTypeList;
import elisa_client.model.Option;
import elisa_client.model.OptionList;
import elisa_client.model.OptionMetadata;
import elisa_client.model.OptionMetadataList;
import elisa_client.model.SystemAffected;
import elisa_client.model.SystemAffectedList;
import elisa_client.service.ElisaApiException;
import elisa_client.service.ElisaClient;
import static org.junit.Assert.*;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * Unit test for simple App.
 */
public class APITest
{
	private static final Log log = LogFactory.getLog(APITest.class.getName());
	public Jaxb2Marshaller jaxb2Test;
 
	/**
     * Rigourous Test :-)
     */
    @Test
    public void testApp()
    {
        System.out.println("dummy dummy");
    	assertTrue( true );
    }

    @Test
    public void getMessage()
    {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"applicationContext.xml" );
		ElisaClient elisaClient = applicationContext.getBean( "elisaClient", ElisaClient.class );
		if(elisaClient!=null){
		//Message msg = elisaClient.getMessageNoAuthentication(133104);
		//System.out.println( "Message: " + msg.getBody() );
		
		Message msgA;
		try {
			//msgA = elisaClient.getMessage("atlog","el0g_2013",null, 133209);
			msgA = elisaClient.getMessage(null,null,"/home/alina/Desktop/Nssocookie.txt",133221);
			System.out.println( "getMessage body: " + msgA.getBody() );
			System.out.println( msgA.getMessageType() );
			//assertEquals(133104, msgA.getID());
			/*System.out.println( msgA.getOptionalAttributes().getList().size() );
			
			for(int i=0;i<msgA.getOptionalAttributes().getList().size();i++) {
                System.out.println( ((Option)msgA.getOptionalAttributes().getList().get(i)).getName() );
                System.out.println( ((Option)msgA.getOptionalAttributes().getList().get(i)).getValue());
                
                OptionList resultInnerOptions = ((Option)msgA.getOptionalAttributes().getList().get(i)).getOptions();
                System.out.println(resultInnerOptions.getList().size());                   
            }*/
		} catch (ElisaApiException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println(e.getMessage());
			assertFalse(true);
		}				
		}
		else{System.out.println("elisaClient bean is null");}
    }

	@Test
    public void insertMessage()
	{
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"applicationContext.xml" );
		ElisaClient elisaClient = applicationContext.getBean( "elisaClient", ElisaClient.class );		
		if(elisaClient!=null){
						
			InputMessage im = new InputMessage();
	        im.setBody("new msg from java client api");
	        im.setMessageType("Shift Summary");
	        im.setSubject("SHSum client api java message");
	        im.setAuthor("Alina");
	        List<String> sa =  new LinkedList<String>();
	        sa.add("DAQ");
	        sa.add("BCM");
	        //sa.add("dd");
	        im.setSystemsAffected(new SystemAffectedList(sa));
	        im.setStatus("closed");
	        
	        Option shsum = new Option();
	        shsum.setName("ShiftSummary_Desk");
	        shsum.setValue("Run Control");
	        
	        /*Option triggerG = new Option();
	        triggerG.setName("Trigger_Group");
	        triggerG.setValue("Calo");
	        Option triggerId = new Option();
	        triggerId.setName("Trigger_Group");
	        triggerId.setValue("ID");
	        
	        List<Option> innerlist = new LinkedList<Option>();
	        innerlist.add(triggerG);
	        innerlist.add(triggerId);
	        triggerArea.setOptions(new OptionList(innerlist));*/
	        
	        List<Option> list = new LinkedList<Option>();
	        list.add(shsum);
	        im.setOptions(new OptionList(list));
	        
	        //Message msgC = elisaClient.insertMessageNoAuth(im);	
	        //System.out.println( "MessageC: " + msgC.getBody());	
			
	        Message msgB;
			try {
				//msgB = elisaClient.insertMessage("atlog","el0g_2013",null, im);
				msgB = elisaClient.insertMessage(null,null,"/home/alina/Desktop/ssocookie.txt",im);
				System.out.println( "insertMessage body: " + msgB.getBody() );
				assertEquals("Alina", msgB.getAuthor());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				System.out.println(e.getMessage());
				assertFalse(true);
			}
		}
		else{System.out.println("elisaClient bean is null");}

	}
    @Test
    public void getMessageTypes()
    {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"applicationContext.xml" );
		ElisaClient elisaClient = applicationContext.getBean( "elisaClient", ElisaClient.class );
		if(elisaClient!=null){
			MessageTypeList msgTypes;
			try {
				msgTypes=elisaClient.getMessageTypes(null, null, null);
				//msgTypes = elisaClient.getMessageTypes("atlog","el0g_2013",null);
				//msgTypes = elisaClient.getMessageTypes(null,null,"/home/alina/Desktop/ssocookie.txt");
				System.out.println( "Possible message types: ");
				for(int i=0; i<msgTypes.getList().size();i++){
					System.out.println(i+". "+msgTypes.getList().get(i) );	
				}       
				assertEquals(20, msgTypes.getList().size());
			} catch (ElisaApiException e) {
				System.out.println(e.getMessage());
				assertFalse(true);
			}		
		}
		else{System.out.println("elisaClient bean is null");}
    }

    @Test
    public void getMessageTypeOptions()
    {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"applicationContext.xml" );
		ElisaClient elisaClient = applicationContext.getBean( "elisaClient", ElisaClient.class );
		
		if(elisaClient!=null){
			OptionMetadataList opts;			
			try {
				String mt="Data Quality";
				System.out.println( "Options for message type: "+mt);
				//opts = elisaClient.getMessageTypeOptions("atlog","el0g_2013",null,mt);
				//String optsstring = elisaClient.getMessageTypeOptionsxml("atlog","el0g_2013",null,mt);
				//System.out.println(optsstring);
				//opts = elisaClient.getMessageTypeOptions(null,null,"/home/alina/Desktop/ssocookie.txt",);
				opts = elisaClient.getMessageTypeOptions(null,null,null,mt);
				
				if(opts.getList().size()!=0){
					
				List<OptionMetadata> options=opts.getList();
				for(OptionMetadata option: options) {
					System.out.println(option.getName()+
							" "+option.getType()+
							" "+option.getPossibleValues()+
							" "+option.getComment());
					if(option.getChildOptions()!=null){
			            log.info("Get inner list size: "+option.getChildOptions().getList().size());
						List<OptionMetadata> inopts=option.getChildOptions().getList();
			            for(OptionMetadata ino: inopts) {
							System.out.println(ino.getName()+
									" "+ino.getType()+
									" "+ino.getPossibleValues()+
									" "+ino.getComment());
			            }
					}    
				}
				}
			} catch (ElisaApiException e) {
				System.out.println(e.getMessage());
			}
		}
		else{System.out.println("elisaClient bean is null");}
    }

    @Test
    public void getMessageTypeSA()
    {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"applicationContext.xml" );
		ElisaClient elisaClient = applicationContext.getBean( "elisaClient", ElisaClient.class );
		
		if(elisaClient!=null){
			SystemAffectedList sa;
			try {
				//sa = elisaClient.getSystemsAffected("atlog","el0g_2013",null,MessageType.RUN_CONTROL);
				//sa = elisaClient.getSystemsAffected(null,null,"/home/alina/Desktop/ssocookie.txt",MessageType.TRIGGER);
				sa = elisaClient.getSystemsAffected(null,null,null,"Trigger");
				
				System.out.println( "Preset SA for message type "+"Trigger");
				for(int i=0; i<sa.getList().size();i++){
					System.out.println(sa.getList().get(i));
				}
			} catch (ElisaApiException e) {
				System.out.println(e.getMessage());
			}
		}
		else{System.out.println("elisaClient bean is null");}
    }

    @Test
    public void getAllSA()
    {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"applicationContext.xml" );
		ElisaClient elisaClient = applicationContext.getBean( "elisaClient", ElisaClient.class );
		
		if(elisaClient!=null){
			SystemAffectedList sa;;
			try {
				sa = elisaClient.getSystemsAffected(null,null,null,null);
				//sa = elisaClient.getSystemsAffected("atlog","el0g_2013",null,mt);
				//sa = elisaClient.getSystemsAffected(null,null,"/home/alina/Desktop/ssocookie.txt",null);
				System.out.println( "All possible SA");
				for(int i=0; i<sa.getList().size();i++){
					System.out.println(sa.getList().get(i));
				}
				assertEquals(36, sa.getList().size());
			} catch (ElisaApiException e) {
				System.out.println(e.getMessage());
			}
		}
		else{System.out.println("elisaClient bean is null");}
    }
    @Test
    public void marshall_unmarshall()
    {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(	"applicationContext.xml" );
		ElisaClient elisaClient = applicationContext.getBean( "elisaClient", ElisaClient.class );
		jaxb2Test = applicationContext.getBean( "jaxb2Test", Jaxb2Marshaller.class );
		if(elisaClient!=null){			
			//create a test OptionMetadataList to marshall
			List<OptionMetadata> top_test_om = new LinkedList<OptionMetadata>();
	        
	        OptionMetadata test_om=new OptionMetadata();
	        test_om.setID(1234);
	        test_om.setName("TEST OPTION_METADATA");
	        test_om.setPossibleValues("one,two,three");
	        test_om.setType("SINGLEVALUE");

	        OptionMetadata test_om_inner=new OptionMetadata();
	        test_om_inner.setID(5678);
	        test_om_inner.setName("INNER TEST OPTION_METADATA");
	        test_om_inner.setPossibleValues("five, six, seven, eight");
	        test_om_inner.setType("MULTIPLEVALUE");
	        
	        List<OptionMetadata> list_test_om = new LinkedList<OptionMetadata>();
	        list_test_om.add(test_om_inner);
	        test_om.setChildOptions(list_test_om);
	        top_test_om.add(test_om);
	        
	        OptionMetadataList top_test_oml=new OptionMetadataList(top_test_om);

	        //Get a XML representation of the object using JAXB2
	        String xml = toXML(top_test_oml);
			log.info("OptionMetadata in XML:\n"+xml);
	        
			//unmarshall back to object
			//ImmutableOptionMetadataList oml_back = (ImmutableOptionMetadataList)fromXML(xml);
			OptionMetadataList oml_back = (OptionMetadataList)fromXML(xml);
				if(oml_back.getList().size()!=0){
			
					
				List<OptionMetadata> options=oml_back.getList();
				for(OptionMetadata option: options) {
					System.out.println("TEST: "+option.getName()+
							" "+option.getType()+
							" "+option.getPossibleValues()+
							" "+option.getComment());
					if(option.getChildOptions()!=null)
			            System.out.println("Get inner list size: "+option.getChildOptions().getList().size());
					else System.out.println("TEST: no inner list");
				}
				}
			}
	
		else{System.out.println("elisaClient bean is null");}
    }

    private String toXML(Object inp) {
        
        DOMResult dom = new DOMResult();
        jaxb2Test.marshal((Object)inp, dom);
        StringWriter writer = new StringWriter();
        Transformer transformer;
        String xml="";
        
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(dom.getNode()), new StreamResult(writer));
             xml = writer.toString();
             
        } catch (TransformerConfigurationException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (TransformerFactoryConfigurationError e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return xml;

    }
    private Object fromXML(String xml) {
        if(xml == null)
            throw new javax.validation.ValidationException("No valid XML representation of the object found in the request");
        StringReader xmlReader = new StringReader(xml);
        StreamSource xmlSource = new StreamSource(xmlReader);
        
        Object m = jaxb2Test.unmarshal(xmlSource);
        if((m==null))
                throw new javax.validation.ValidationException("No valid XML representation of the object found in the request");
        return m;
    }

}
